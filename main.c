/*******************************************
 *               ASCII WEEB                *
 *         ryukoposting.tumblr.com         *
 *                                         *
 *               GNU GPL v3                *
 *                                         *
 * Thank you to the wonderful folks behind *
 *    the creation of the stb libraries    *
 *     https://github.com/nothings/stb     *
 *******************************************/
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <curl/curl.h>

#define STB_IMAGE_IMPLEMENTATION
#define STBI_FAILURE_USERMSG
#include "stb/stb_image.h"

char gradient[] = "%8m#EVnxIFLc|!+=~*-\":'. ";

char reducedGradient[] = "%mEVnFLc!+=*-\":'. ";

int main(int argc, char *argv[]) {
    int inverted = 0, reduced = 0, indexOfUrl = -1, indexOfWaifu = -1, scale = -1;
    if((argc < 2) || (argc > 5)) {
        printf("usage:\n");
        printf("   ascii_weeb <waifu name>\n");
        printf("       loads a random image from the manifest corresponding to the given waifu name.\n\n");
        printf("   ascii_weeb <http://website.com/jpeg_image.jpg>\n");
        printf("       loads the image at the specified url and generates ASCII art of it.\n\n");
        printf("   ascii_weeb <waifu name> <http://website.com/jpeg_image.jpg>\n");
        printf("       loads the image at the specified url and generates ASCII art of it.\n");
        printf("       adds the specified image url and corresponding waifu name to the manifest.\n\n");
        printf("   ascii_weeb -i\n");
        printf("       use the inverted pallette. All directives in above commands are still usable.\n");
        printf("   ascii_weeb -r\n");
        printf("       use a reduced set of characters.\n");
        printf("   ascii_weeb -s=<integer>\n");
        printf("       scale the output to 1/n size. Larger number = smaller output. Default = 1.\n");
        return 0;
    }

    for(unsigned short i = 1; i < argc; i++) {
        size_t len = strlen(argv[i]);
        if(len == 2) {
            if(strcmp(argv[i], "-i") == 0) {
                inverted = 1;
            } else if(strcmp(argv[i], "-r") == 0) {
                reduced = 1;
            } else {
                printf("Incorrect parameters for ascii_weeb. Try entering 'ascii_weeb' for usage help.\n");
                return 0;
            }
        } else if(strncmp(argv[i], "http", 4) == 0) {
            if(indexOfUrl < 0) {
                indexOfUrl = i;
            } else {
                printf("Incorrect parameters for ascii_weeb (multiple URLs detected). Try entering 'ascii_weeb' for usage help.\n");
                return 0;
            }
        } else if((strncmp(argv[i], "-s=", 3) == 0) && (strlen(argv[i]) > 3)) {
            scale = atoi(argv[i] + 3);
            if(scale < 1) scale = 1;
        } else if(indexOfWaifu < 0) {
            indexOfWaifu = i;
        } else {
            printf("Incorrect parameters for ascii_weeb. Try entering 'ascii_weeb' for usage help.\n");
            return 0;
        }
    }

    // seems to behave more randomly w/pointer added to seed
    unsigned int seed = time(NULL);
    srand(seed + (unsigned long)(&inverted));
    char url[256] = "";

    if(indexOfUrl > 0) {
        strcpy(url, argv[indexOfUrl]);

        // add image to manifest if both url index and waifu index are specified
        if(indexOfWaifu > 0) {
            FILE *manifest = fopen("weeb-manifest.txt", "a");
            if(manifest != NULL) {
                fprintf(manifest, "%s %s\n", argv[indexOfWaifu], argv[indexOfUrl]);
                fclose(manifest);
            } else {
                printf("failed to write to manifest!\n");
            }
        }
    } else {
        // no url specified, load one from manifest
        char line[256];
        char waifuBuffer[16384] = "";
        unsigned long urlEnds[64] = {};
        unsigned int urlsFound = 0;
        FILE *manifest = fopen("weeb-manifest.txt", "r");

        while (fgets(line, sizeof(line), manifest) != NULL) {
            line[strlen(line) - 1] = '\0'; // eat the newline fgets() stores
            unsigned int i = 0;
            //int isWaifu = (sizeof(argv[indexOfUrl]) == (strchr(line, ' ') - line));
            int isWaifu = 1;
            while ((isWaifu) && (line[i] != ' ') && (i < sizeof(line))) {
                if (line[i] != argv[indexOfWaifu][i]) isWaifu = 0;
                i++;
            }

            if (isWaifu) {
                strcat(waifuBuffer, strchr(line, ' ') + 1);
                urlEnds[urlsFound] = strlen(waifuBuffer);
                urlsFound++;
            }
        }

        int roll = rand() % urlsFound;
        waifuBuffer[urlEnds[roll]] = 0;
        if(roll == 0) {
            strcpy(url, waifuBuffer);
        } else {
            strcpy(url, waifuBuffer + urlEnds[roll - 1]);
        }
    }

    char filename[16] = "dl";
    char *extension = strrchr(url, '.');
    *(extension + 4) = 0;
    strcat(filename, extension);

    CURL* easyhandle = curl_easy_init();
    curl_easy_setopt(easyhandle, CURLOPT_URL, url);

    FILE* file = fopen(filename, "w");
    curl_easy_setopt( easyhandle, CURLOPT_WRITEDATA, file) ;
    curl_easy_perform( easyhandle );
    curl_easy_cleanup( easyhandle );
    fclose(file); //make sure to close file

    // file has been saved to dl.*
    int x, y, n;
    unsigned char *data = stbi_load_image(filename, &x, &y, &n, 1);

    if(scale == -1) {
        struct winsize w;
        ioctl(0, TIOCGWINSZ, &w);
        scale = 1;
        while((x / scale) > (w.ws_col)) scale++;
    }

    if(stbi_failure_reason() != NULL) {
        printf("Image interpretation failed! %s\n", stbi_failure_reason());
        curl_easy_cleanup(easyhandle);
        fclose(file);
        return 1;
    } else {
        printf("%s (%s), %dx%d (1:%d)\n", filename, url, x, y, scale);

        unsigned long imgWidth = x / scale;

        for(unsigned int row = 0; row < y; row += scale) {
            for(unsigned int col = 0; col < x; col += scale) {
                unsigned long pos = (row * x) + col;
                unsigned short thisIndex = data[pos] / (reduced ? 13 : 10);
                unsigned short maxIndex = (reduced ? 18 : 24);
                if(thisIndex > maxIndex) thisIndex = maxIndex;

                if(reduced) {
                    printf("%c", (inverted) ? reducedGradient[18 - thisIndex] : reducedGradient[thisIndex]);
                } else {
                    printf("%c", (inverted) ? gradient[24 - thisIndex] : gradient[thisIndex]);
                }
            }
            printf("\n");
        }
    }

    //remove(filename);
    fclose(file);

    return 0;/* all done */
}
