cmake_minimum_required(VERSION 3.6.2)
project(ascii_weeb C)

set(CMAKE_C_STANDARD 11)

set(CURL_LIBRARY "-lcurl")
find_package(CURL REQUIRED)
add_executable(ascii_weeb main.c stb/stb_image.h)
include_directories(${CURL_INCLUDE_DIR})
target_link_libraries(ascii_weeb ${CURL_LIBRARIES})
