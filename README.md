ascii_weeb will happily compile in GCC. I used Cygwin, though MinGW and plain ol'
GCC should both work fine. You'll need libcurl installed in order to compile.

**Key Features**
 - Load image from web URLs
 - Compatible with JPEG and PNG
 - Saves URLs for easy access in the future
 - Multiple options for output style, including inversion and scaling
 - Automatically scales output to terminal size

**Examples**
```
ascii_weeb http://website.com/path/to/a/jpeg/file.jpg
```

Will load the JPEG file from the url and make it into ASCII art in your terminal.
The output is automatically scaled to the size of your terminal. If you want to
set your own scaling, use
```
ascii_weeb -s=<integer>
```
which will scale the output in a 1:n ratio based on the given number. For example,
-s=2 will output the image in 1/2 scaling (1:1 is the max size).
```
ascii_weeb -i http://website.com/path/to/a/jpeg/file.jpg
```
Will do the exact same thing, except the "dark" and "light" characters are reversed.
```
ascii_weeb ryuko https://i.imgur.com/AOxdjKM.jpg
```
Will generate the art, but it will also save the url to its manifest.

ascii_weeb can retrieve image paths from a text file `weeb-manifest.txt`,
which will be put in the same directory as the executable. Here is a sample
of what this file might look like:
```
ryuko https://i.imgur.com/AOxdjKM.jpg
ryuko https://i.imgur.com/06PFILR.jpg
ryuko https://i.imgur.com/JpZVqWI.jpg
ryuko https://i.imgur.com/ZrtNAci.jpg
ryuko https://i.imgur.com/SohT5ib.jpg
zerotwo https://i.imgur.com/sTnj9gJ.jpg
zerotwo https://i.imgur.com/KeUn8ze.jpg
```
With this manifest, you can now run
```
ascii_weeb ryuko
```
and ASCII art will be generated based on a randomly-selected picture of ryuko.

**Screenshots**

![https://i.imgur.com/eYE8nQH.png](https://i.imgur.com/eYE8nQH.png)

![https://i.imgur.com/1viC4GP.png](https://i.imgur.com/1viC4GP.png)

![https://i.imgur.com/y03RSaS.png](https://i.imgur.com/y03RSaS.png)
